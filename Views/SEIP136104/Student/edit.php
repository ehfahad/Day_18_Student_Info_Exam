
<?php

include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Student\Student;

$obj =new Student();

$obj->prepare($_GET);
$student = $obj->view();


?>



<html>
<head>
    <title>Update Student Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Student Information</h2>

    <form role="form" method="post" action="update.php">

        <input type="hidden" name="id" value="<?php echo $_GET['id']?>">
        <label>Enter First Name : </label>
        <input type="text" name="first" value="<?php echo $student->firstname ?>">
        <br>
        <label>Enter Middle Name : </label>
        <input type="text" name="middle" value="<?php echo $student->middlename ?>">
        <br>
        <label>Enter Last Name : </label>
        <input type="text" name="last" value="<?php echo $student->lastname ?>">
        <br>
        <input type="submit" value="Submit">
    </form>
</div>

</body>
</html>



