<?php
session_start();

include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Student\Student;
use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

$obj =new Student();
//$obj->prepare($_POST);
$allStudent = $obj->index();
//Utility::debug($allStudent);

?>

<html>
    <head>
        <title>Student Information</title>
        <title>Insert Student Details</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </head>


    <body>

    <h3><center>Students Information</center></h3>
    <br>
    <br>
        <div class="container">
            <div id="message">
                <center><b><?php echo Message::message() ?></b></center>

            </div>
            <a href="create.php" class="btn btn-primary">Create Again</a>
            <table class="table">
                <thead>
                    <tr>
                        <td>SL</td>
                        <td>ID</td>
                        <td>FirstName</td>
                        <td>LastName</td>
                        <td>MiddleName</td>
                        <td>Action</td>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <?php $sl =0;
                                foreach ($allStudent as $student) { $sl++ ?>
                        <td> <?php echo $sl?> </td>
                        <td><?php echo $student->id ?></td>
                        <td><?php echo $student->firstname ?></td>
                        <td><?php echo $student->lastname ?></td>
                        <td><?php echo $student->middlename ?></td>
                        <td>


                            <a href="create.php" class="btn btn-primary">Create</a>
                            <a href="view.php?id=<?php echo $student->id ?>" class="btn btn-info">View</a>
                            <a href="edit.php?id=<?php echo $student->id ?>" class="btn btn-warning">Update</a>
                            <a href="delete.php?id=<?php echo $student->id ?>" Onclick="return confirmDelete()" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>\

    <script>
        $('#message').show().delay(1100).fadeOut();

        function confirmDelete(){
            var x = confirm("Sure to delete?");
            if (x)
            return true;
            else
            return false;

        }

    </script>
    </body>

</html>