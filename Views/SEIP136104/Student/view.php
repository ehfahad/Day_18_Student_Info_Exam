
<?php

include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Student\Student;

$obj =new Student();

$obj->prepare($_GET);
$student = $obj->view();


?>


<html>
<head>
    <title>View Student Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Student Information</h2>

    <label>Student ID </label>
    <?php echo $student->id?>
    <br>
        <label>Student First Name : </label>
        <?php echo $student->firstname?>
        <br>
        <label>Student Middle Name : </label>
    <?php echo $student->middlename?>
        <br>
        <label>Student Last Name : </label>
    <?php echo $student->lastname?>
        <br>
    <a href="index.php" class="btn btn-primary">Done</a>
    </form>
</div>

</body>
</html>



