<?php

namespace App\Bitm\SEIP136104\Utility;



class Utility{
    public static function redirect($data){
        header("Location:".$data);
    }

    public static function debug($data){
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }
}