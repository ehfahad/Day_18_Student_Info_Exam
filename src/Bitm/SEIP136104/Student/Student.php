<?php

namespace App\Bitm\SEIP136104\Student;

use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

class Student{
    public $id= '';
    public $conn;
    public $first='';
    public $middle='';
    public $last='';

    public function prepare($data){
        if(array_key_exists("id",$data))
            $this->id = $data['id'];

        if(array_key_exists("first",$data))
            $this->first = $data['first'];

        if(array_key_exists("middle",$data))
            $this->middle = $data['middle'];

        if(array_key_exists("last",$data))
            $this->last = $data['last'];
    }


    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "labxm5b22");
    }



    public function index(){
        $allStudent = array();
        $query = "SELECT * FROM `student`";
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $allStudent[] = $row;

        return $allStudent;
    }


    public function store(){
        $query = "INSERT INTO `labxm5b22`.`student` (`firstname`, `middlename`, `lastname`) VALUES ('".$this->first."', '".$this->middle."', '".$this->last."')";
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");
        }
        else
            echo "error";
    }

    public function view(){
        $allStudent = array();
        $query = "SELECT * FROM `student` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($row= mysqli_fetch_object($result))
            return $row;
        else
            echo "error";
    }


    public function delete(){
        $query = "DELETE FROM `labxm5b22`.`student` WHERE `student`.`id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been deleted successfully");
            Utility::redirect("index.php");
        }
        else
            echo "error";
    }


    public function update(){
        echo $this->id.$this->first.$this->middle.$this->last;
        $query = "UPDATE `labxm5b22`.`student` SET `firstname` = '".$this->first."', `middlename` = '".$this->middle."', `lastname` = '".$this->last."' WHERE `student`.`id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been updated successfully");
            Utility::redirect("index.php");
        }
        else
            echo "error";
    }
}